package com.example.webview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {
    WebView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view = (WebView) this.findViewById(R.id.webView);
        view.getSettings().setJavaScriptEnabled(true);
        view.setWebViewClient(new MyBrowser());
        view.loadUrl("http://www.google.com");
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            //when touched by the back button
            if ((keyCode == KeyEvent.KEYCODE_BACK) && view.canGoBack()) {
                view.goBack(); //method goback(),to return to the previous page
                return true;
            }

            // If no page has been opened
            // it will exit the activity (close the application)
            return super.onKeyDown(keyCode, event);
        }
    }

